import React, { lazy } from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';

const App: React.FC = () => {
  return (
    <Switch>
      <Route exact path="/" component={lazy(() => import('pages/Home'))} />
      <Route
        exact
        path="/login"
        component={lazy(() => import('pages/Login'))}
      />
    </Switch>
  );
};

export default withRouter(App);
