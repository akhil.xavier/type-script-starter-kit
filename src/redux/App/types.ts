export const types = {
  DEFAULT: 'DEFAULT',
  DEFAULT_1: 'DEFAULT_1',
};

interface DefaultAction {
  type: typeof types.DEFAULT;
  payload: String;
}
interface DefaultActionTwo {
  type: typeof types.DEFAULT_1;
  payload: String;
}

export type DefaultTypes = DefaultAction | DefaultActionTwo;
